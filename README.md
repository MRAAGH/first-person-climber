# First Person Climber

My first Unity project! Generates a bunch of intertwining spiraling paths you can walk on! It has a lot of configuration parameters. At the end of this document is a description of these parameters.

![screenshot](https://img.ourl.ca/firstpersonclimber.png)

# Downloads

- Linux 64-bit (13.7 MB):
https://mazie.rocks/files/FirstPersonClimber_linux.zip

- Windows 64-bit (43.3 MB):
https://mazie.rocks/files/FirstPersonClimber_windows.zip

# How to compile

- Windows is recommended for this, because Linux Unity Editor is experimental
- Go to [Unity download archive](https://unity3d.com/get-unity/download/archive and download) and download Unity 5.3.5.f1 installer.
- Install the unity editor and build tools for the platform you want to build for
- You need a Unity account before they allow you to open the editor
- Open this project in Unity Editor 5.3.5.f1
- File/Build
- Add `config.txt` next to the executable you made (is not auto-created)

# Parameter description

These are the generation parameters which you can set in config.txt

Parameter | Meaning
--- | ---
Seed | world seed
RandomSeed | if true, pick random seed instead
StartAngle | the direction of the very first path
PathCount | TOTAL number of path segments that will be generated
Endpoints | if true, put little spheres where paths connect
UseAltPrefab | dark theme
SmartTurns | when connecting to existing pillar, use information from the pillar to choose turn direction
DeviateChance | chance of changing direction when connecting to existing pillar
SplitChance | chance that path will split when there is an opportunity (opportunities are kinda rare though)
SplitLimit | limit number of splits in the entire generation process
ChangeDirectionChance | chance of spontaneously changing direction
PathWidth | dimensions of the path box
PathThickness | dimensions of the path box
PillarSpace | space between pillar and adjacent path
PillarThicknessMin | lower bound for pillar thickness
PillarThicknessMax | upper bound for pillar thickness
PillarThicknessFirst | thickness of first pillar
PillarMaxHeightMultiplier | allowed pillar height determined by multiplying this by thickness
PilarMaxHeight | absolutely upper bound for pillar height
Angle | determines the acceptable angles of bends on the path
Angle/min | minimum angle
Angle/max | maximum angle
Angle/marginmin | the margin below minimum that we still allow in special situations
Angle/marginmax | the margin above maximum that we still allow in special situations
Slope | same structure as for the angle, but this determines the allowed steepness of the slope
Shadow | same thing again, this time limits for horizontal length of segments (length of shadow if sun is straight up)
VisualsA | is supposed to be the "decoration" controls, but seems to be broken!
VisualsB | "decoration" controls for type B pillars. Also seems broken!
