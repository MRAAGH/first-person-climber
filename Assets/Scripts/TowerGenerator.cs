﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class TowerGenerator : MonoBehaviour {

    private Util u = new Util();

    private TowerParameters p;

    private PillarChoices pillars;

    private SequencePriorityQueue queue;
    private int splitCount { get { return (queue == null ? 0 : queue.Count - 1); } }

    //A constant which is calculated from the tower parameters and which I want to calculate only once:
    private float extentensionRatio;

    private int currPathCount;
    
    public void Initialize(TowerParameters towerparameters)
    {
        p = towerparameters;
        pillars = new PillarChoices(new PillarChoiceParameters(p), p.PillarVisualParameters);
        queue = new SequencePriorityQueue();

        //Calculate the ratio between distance from the pillar and the
        //required extension to the path leading to it to make the turn look nicer
        extentensionRatio = 1f / Mathf.Tan(Mathf.Deg2Rad * (90f - (p.Angle.Max + p.Angle.Min) / 4f));
        p.Angle.Check("Angle");
        p.Slope.Check("Slope");
        p.Shadow.Check("Shadow");
    }

    public void FirstStep()
    {
        generateFirstPath();
    }
    
    public void NextStep()
    {
        generateNewPath();
    }

    public void Decorate()
    {
         handleInstances(pillars.Decorate());
    }

    //BUILDING:

    private void generateFirstPath()
    {
        //Make the first path:
        float shadowLength = u.ran(p.Shadow.Min, p.Shadow.Max);
        float height = u.ran(u.slopeToHeight(p.Slope.Min, shadowLength), u.slopeToHeight(p.Slope.Max, shadowLength));
        Vector2 shadow = u.angleToUnitVector2(p.StartAngle) * shadowLength;

        //Make a really massive first pillar:
        float thickness = p.PillarThicknessFirst;
        float distance = thicknessToDistance(thickness);
        //Left or right?
        bool GoingLeft = u.chance(0.5f);
        //Choose pillar position:
        Vector2 location = determineFreshPillarCoords(u.v2(p.StartPosition), p.StartAngle, shadowLength, distance, GoingLeft);
        
        //Finish the first path:
        //Add extension without affecting the pillar:
        float extension = distanceToExtension(distance);
        shadow += shadow / shadow.magnitude * extension;

        //Display:
        Vector3 nextPosition = p.StartPosition + u.v3(shadow, height);
        addPathByEndpoints(p.StartPosition, nextPosition);
        addPillar(location, thickness, nextPosition.y, nextPosition.y);

        //Keep track of generation sequence:
        queue.Push(new Sequence(p.StartAngle, nextPosition, GoingLeft));
    }
    private void generateNewPath()
    {
        //Get data:
        Sequence cur = queue.Pop();

        //Find valid pillars to connect to:
        List<Pillar> validPillars = pillars.FindValid(cur);

        //If there were none, generate a random path and make a pillar next to it:
        if(validPillars.Count == 0)
        {
            buildPathToNewPillar(cur);
        }
        //If there was exactly one, generate a path to it:
        else if (validPillars.Count == 1)
        {
            buildPathToPillar(validPillars[0], cur);
        }
        //If there were several ...
        else
        {
            //Choose one of the valid pillars:
            int chosen = (int)(UnityEngine.Random.value * validPillars.Count);

            //Decide whether to make a split here:
            if(u.chance(p.SplitChance) && splitCount < p.SplitLimit)
            {
                //Another path from here.
                //Choose a pillar which is not the same as the one we just built the path to:
                int chosen2;
                do
                {
                    chosen2 = (int)(UnityEngine.Random.value * validPillars.Count);
                } while (chosen2 == chosen);
                //A whole new generation sequence:
                Sequence other = new Sequence(cur.Angle, cur.Pos, cur.GoingLeft);
                queue.Push(other);
                buildPathToPillar(validPillars[chosen2], other);
            }
            buildPathToPillar(validPillars[chosen], cur);
        }
    }
    private void buildPathToNewPillar(Sequence cur)
    {
        //Make the path:
        //Random angle:
        float angleChange = u.ran(p.Angle.Min, p.Angle.Max);
        if (cur.GoingLeft)
            cur.Angle += angleChange;
        else
            cur.Angle -= angleChange;
        //Random distance:
        float shadowLength = u.ran(p.Shadow.Min, p.Shadow.Max);
        //Random height:
        float height = u.ran(u.slopeToHeight(p.Slope.Min, shadowLength), u.slopeToHeight(p.Slope.Max, shadowLength));
        Vector2 shadow = u.angleToUnitVector2(cur.Angle) * shadowLength;

        //Make a pillar next to it:
        float thickness = pillarThicknessGen();
        float distance = thicknessToDistance(thickness);
        //Change direction?
        if (UnityEngine.Random.value < p.ChangeDirectionChance)
        {
            cur.GoingLeft = !cur.GoingLeft;
        }
        //Choose pillar position:
        Vector2 location = determineFreshPillarCoords(u.v2(cur.Pos), cur.Angle, shadowLength, distance, cur.GoingLeft);

        //Finish the path:
        //Add extension without affecting the pillar:
        float extension = distanceToExtension(distance);
        shadow += shadow / shadow.magnitude * extension;

        //Display:
        Vector3 nextPosition = cur.Pos + u.v3(shadow, height);
        addPathByEndpoints(cur.Pos, nextPosition);
        Pillar q = addPillar(location, thickness, nextPosition.y, nextPosition.y);

        //Keep track of generation sequence:
        cur.Pos = nextPosition;

        //Acknowledge pillar visit:
        q.Visit(cur.GoingLeft);
    }
    private void buildPathToPillar(Pillar q, Sequence cur)
    {
        //Left or right?
        if (u.chance(p.DeviateChance))
        {
            cur.GoingLeft = !cur.GoingLeft;
        }
        if (p.SmartTurns)
        {
            //Allow the pillar to recommend a direction:
            cur.GoingLeft = q.RecommendDirection(cur.GoingLeft);
        }

        //Make a path:
        float distance = thicknessToDistance(q.Thickness);
        Vector2 directShadow = q.Location - u.v2(cur.Pos);
        float directAngle = u.vector2ToAngle(directShadow);
        Vector2 shadow = nextToPillarCoords(Vector2.zero, directAngle, directShadow.magnitude, distance, cur.GoingLeft);
        float shadowLength = shadow.magnitude;
        //Choose the path's height change:
        float minHeight = u.maxof(u.slopeToHeight(p.Slope.Min, shadowLength), q.Bottom - cur.Pos.y);
        float maxHeight = u.minof(u.slopeToHeight(p.Slope.Max, shadowLength), q.Maxtop - cur.Pos.y);
        float height = u.ran(minHeight, maxHeight);

        //Finish the path:
        //Add extension without affecting the pillar:
        float extension = distanceToExtension(distance);
        shadow += shadow / shadow.magnitude * extension;

        //Display:
        Vector3 nextPosition = cur.Pos + u.v3(shadow, height);
        addPathByEndpoints(cur.Pos, nextPosition);
        q.NewTop(height + cur.Pos.y); //Fix the pillar

        //Keep track of generation sequence:
        cur.Pos = nextPosition;
        cur.Angle = u.vector2ToAngle(shadow);

        //Acknowledge pillar visit:
        q.Visit(cur.GoingLeft);
    }

    //Display:

    //Place a path given the coordinates of its endpoints:
    private void addPathByEndpoints(Vector3 a, Vector3 b)
    {
        if (p.Endpoints)
        {
            Instantiate(p.EndpointPrefab, a, Quaternion.identity);
            Instantiate(p.EndpointPrefab, b, Quaternion.identity);
        }

        Vector3 c = a - b;
        Vector2 shadow = new Vector2(c.x, c.z);
        float length = c.magnitude;
        float shadowlength = shadow.magnitude;
        Vector3 center = (a + b) / 2;

        float rotatehor = (c.x == 0 ? 90 : Mathf.Rad2Deg * Mathf.Atan(c.z / c.x));
        float rotatever = (shadowlength == 0 ? 90 : Mathf.Rad2Deg * Mathf.Atan(c.y / shadowlength));
        if (c.x <= 0) rotatever = -rotatever; //I had an issue and I felt this line would fix it. It does, but I still don't know why it happened ...

        Quaternion rotation = Quaternion.Euler(0, -rotatehor, 0) * Quaternion.Euler(0, 0, rotatever);

        addPath(center, new Vector3(length, p.PathThickness, p.PathWidth), rotation);
    }
    private Pillar addPillar(Vector2 location, float thickness, float bottom, float top)
    {
        Vector3 position = u.v3(location);

        GameObject prefabToUse = (p.UseAltPrefab && pillars.Count % 10 == 0) ? p.PillarPrefabAlt : p.PillarPrefab;
        //Height is 0 for now. The Pillar object will handle that.
        GameObject go = addSegment(prefabToUse, position, new Vector3(thickness, 0, thickness), Quaternion.identity, "Pillar");
        Pillar thispillar = new Pillar(p.PillarParameters, go, location, thickness, bottom, top);
        thispillar.NewTop(top);
        pillars.Add(thispillar);
        return thispillar;
    }
    private void addPath(Vector3 position, Vector3 scale, Quaternion rotation)
    {
        GameObject prefabToUse = (p.UseAltPrefab && currPathCount % 10 == 0) ? p.PathPrefabAlt : p.PathPrefab;
        addSegment(prefabToUse, position, scale, rotation, "Path");
    }
    private GameObject addSegment(GameObject prefab, Vector3 position, Vector3 scale, Quaternion rotation, string name)
    {
        GameObject go = (GameObject)Instantiate(prefab, position, rotation);
        go.transform.localScale = scale;
        go.transform.parent = this.transform;
        go.name = name;
        return go;
    }
    //Execute a number of ToDoLists:
    private void handleInstances(ToDoList[] lists)
    {
        foreach (ToDoList tdl in lists)
        {
            while (!tdl.IsDone)
            {
                Instantiator i = tdl.Next;
                GameObject go = (GameObject)Instantiate(i.Prefab, i.Position, i.Rotation);
                go.transform.parent = i.Parent;
                go.name = i.Name;
            }
        }
    }

    //PILLAR LOGIC: (mostly in other classes):

    private float pillarThicknessGen()
    {
        //Subject to change.
        return u.ran2(p.PillarThicknessMin, p.PillarThicknessMax);
    }

    //CONVERSIONS & MATH:

    private float thicknessToDistance(float thickness)
    {
        return thickness / 2 + p.PathWidth / 2 + p.PillarSpace;
    }
    private float distanceToExtension(float distance)
    {
        return distance * extentensionRatio;
    }
    private Vector2 determineFreshPillarCoords(Vector2 origin, float angle, float shadowLength, float distance, bool isLeft)
    {
        //Calculate the coordinates of a new pillar based on the parameters of the path leading to it.
        float offsetAngle = Mathf.Rad2Deg * Mathf.Atan(distance / shadowLength);
        float fullAngle = isLeft ? angle + offsetAngle : angle - offsetAngle;
        float fullShl = Mathf.Sqrt(distance * distance + shadowLength * shadowLength);
        return origin + u.angleToUnitVector2(fullAngle) * fullShl;
    }
    private Vector2 nextToPillarCoords(Vector2 origin, float fullAngle, float fullShl, float distance, bool isLeft)
    {
        //Calculate the coordinates of the spot right next to a pillar.
        float offsetAngle = Mathf.Rad2Deg * Mathf.Asin(distance / fullShl);
        float angle = isLeft ? fullAngle - offsetAngle : fullAngle + offsetAngle;
        float shl = Mathf.Sqrt(fullShl * fullShl - distance * distance);

        return origin + u.angleToUnitVector2(angle) * shl;
    }

    //MISC:

    public override string ToString()
    {
        return currPathCount + " paths, " + splitCount + " of " + p.SplitLimit + " splits, " + pillars.Count + " pillars\n"
            + "Origin " + transform.position + " " + transform.rotation.eulerAngles + "\n"
            + p + "\n";
    }
}
