﻿using UnityEngine;
using Random = UnityEngine.Random;

//Handy little pieces of code:

public class Util
{
    public Vector2 v2(Vector3 v)
    {
        return new Vector2(v.x, v.z);
    }
    public Vector3 v3(Vector2 v)
    {
        return new Vector3(v.x, 0f, v.y);
    }
    public Vector3 v3(Vector2 v, float y)
    {
        return new Vector3(v.x, y, v.y);
    }

    public float ran(float min, float max)
    {
        return Random.value * (max - min) + min;
    }
    public float ran2(float min, float max)
    {
        return Random.value * Random.value * (max - min) + min;
    }
    public float ran3(float min, float max)
    {
        return Random.value * Random.value * Random.value * (max - min) + min;
    }
    public bool chance(float prob)
    {
        return Random.value < prob;
    }
    public float minof(float a, float b)
    {
        return a < b ? a : b;
    }
    public float maxof(float a, float b)
    {
        return a > b ? a : b;
    }
    public string shb(bool b)
    {
        return b ? "1" : "0";
    }

    //CONVERSIONS AND CALCULABLES:

    public float slopeToHeight(float slope, float shadowLength)
    {
        return shadowLength * Mathf.Tan(Mathf.Deg2Rad * slope);
    }
    public Vector2 angleToUnitVector2(float angle)
    {
        return new Vector2(Mathf.Cos(Mathf.Deg2Rad * angle), Mathf.Sin(Mathf.Deg2Rad * angle));
    }
    public float vector2ToAngle(Vector2 v)
    {
        return vector2ToAngle(v.x, v.y);
    }
    public float vector2ToAngle(float x, float y)
    {
        //Special cases (vertical):
        if (x == 0)
        {
            if (y < 0)
                return 270;
            if (y == 0)
                return 0;
            return 90;
        }
        //Quadrants 2 and 3:
        if (x < 0)
            return 180 + Mathf.Rad2Deg * Mathf.Atan(y / x);
        //Quadrant 4:
        if (y < 0)
            return 360 + Mathf.Rad2Deg * Mathf.Atan(y / x);
        //Quadrant 1:
        return Mathf.Rad2Deg * Mathf.Atan(y / x);
    }
}