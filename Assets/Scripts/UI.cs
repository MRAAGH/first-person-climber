﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UI : MonoBehaviour {

    [SerializeField]
    private Text Info;
    [SerializeField]
    private PlayerObserver Player;
    [SerializeField]
    private RandomTerrainGen Towers;

    private string content;

    void OnGUI()
    {
        content = Player.report() + "\n\n" + Towers;
        Info.text = content;
    }
}
