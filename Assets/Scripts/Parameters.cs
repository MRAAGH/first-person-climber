using UnityEngine;
using System;

//Collections of parameters for various aspects of the code.
//These appear in the Unity inspector.
//Most of this file was generated using a c++ script rather than written by hand.

[Serializable]
public class PillarVisualParameters
{
    [SerializeField]
    private PillarVisualSubParameters VisualsA_;
    [SerializeField]
    private PillarVisualSubParameters VisualsB_;

    public PillarVisualSubParameters VisualsA { get { return VisualsA_; } }
    public PillarVisualSubParameters VisualsB { get { return VisualsB_; } }

    public PillarVisualParameters(PillarVisualSubParameters visualsa, PillarVisualSubParameters visualsb)
    {
        VisualsA_ = visualsa;
        VisualsB_ = visualsb;
    }

    public void FromJsonObject(JsonParameters obj)
	{
		VisualsA_.FromJsonObject(obj.VisualsA);
		VisualsB_.FromJsonObject(obj.VisualsB);
    }

    public override string ToString()
    {
        return "VA" + VisualsA_ + " VB" + VisualsB_;
    }
}

[Serializable]
public class PillarVisualSubParameters
{
    [SerializeField]
    private GameObject BasePrefab_;
    [SerializeField]
    private GameObject RoofPrefab_;
    [SerializeField]
    private bool Bases_;
    [SerializeField]
    private bool Roofs_;
    [SerializeField]
    private bool Hide_;
    [SerializeField]
    private bool Extend_;
    [SerializeField]
    private bool RequireElevation_;
    [SerializeField]
    private float Required_;
    [SerializeField]
    private bool ExtendInstead_;
    [SerializeField]
    private float SpareT_;
    [SerializeField]
    private float SpareB_;

    public GameObject BasePrefab { get { return BasePrefab_; } }
    public GameObject RoofPrefab { get { return RoofPrefab_; } }
    public bool Bases { get { return Bases_; } }
    public bool Roofs { get { return Roofs_; } }
    public bool Hide { get { return Hide_; } }
    public bool Extend { get { return Extend_; } }
    public bool RequireElevation { get { return RequireElevation_; } }
    public float Required { get { return Required_; } }
    public bool ExtendInstead { get { return ExtendInstead_; } }
    public float SpareT { get { return SpareT_; } }
    public float SpareB { get { return SpareB_; } }

    public PillarVisualSubParameters(GameObject baseprefab, GameObject roofprefab, bool bases, bool roofs, bool hide, bool extend, bool requireelevation, float required, bool extendinstead, float sparet, float spareb)
    {
        BasePrefab_ = baseprefab;
        RoofPrefab_ = roofprefab;
        Bases_ = bases;
        Roofs_ = roofs;
        Hide_ = hide;
        Extend_ = extend;
        RequireElevation_ = requireelevation;
        Required_ = required;
        ExtendInstead_ = extendinstead;
        SpareT_ = sparet;
        SpareB_ = spareb;
    }

    public void FromJsonObject(VisualsJsonParameters obj)
	{
        Bases_ = obj.Bases;
        Roofs_ = obj.Roofs;
        Hide_ = obj.Hide;
        Extend_ = obj.Extend;
        RequireElevation_ = obj.RequireElevation;
        Required_ = obj.Required;
        ExtendInstead_ = obj.ExtendInstead;
        SpareT_ = obj.SpareT;
        SpareB_ = obj.SpareB;
    }

    public override string ToString()
    {
        string ret = "[";
        if (Bases_) ret += "B";
        if (Roofs_) ret += "R";
        if (Hide_) ret += "H";
        if (Extend_) ret += "E";
        if (RequireElevation_)
        {
            ret += Required_;
            if (ExtendInstead_) ret += "E";
        }
        return ret + "+" + SpareT_ + "-" + SpareB_ + "]";
    }
}

[Serializable]
public class PillarParameters
{
    [SerializeField]
    private float MaxHeightMultiplier_;
    [SerializeField]
    private float MaxHeight_;

    public float MaxHeightMultiplier { get { return MaxHeightMultiplier_; } }
    public float MaxHeight { get { return MaxHeight_; } }

    public PillarParameters(float maxheightmultiplier, float maxheight)
    {
        MaxHeightMultiplier_ = maxheightmultiplier;
        MaxHeight_ = maxheight;
    }
	
	public void FromJsonObject(JsonParameters obj)
	{
        MaxHeightMultiplier_ = obj.PillarMaxHeightMultiplier;
        MaxHeight_ = obj.PilarMaxHeight;
	}

    public override string ToString()
    {
        return "*" + MaxHeightMultiplier_ + " +" + MaxHeight_;
    }
}

[Serializable]
public class TowerParameters
{
    [SerializeField]
    private GameObject PathPrefab_;
    [SerializeField]
    private GameObject PillarPrefab_;
    [SerializeField]
    private GameObject PathPrefabAlt_;
    [SerializeField]
    private GameObject PillarPrefabAlt_;
    [SerializeField]
    private GameObject EndpointPrefab_;
    [SerializeField]
    private Vector3 StartPosition_;
    [SerializeField]
    private float StartAngle_;
    [SerializeField]
    private bool Endpoints_;
    [SerializeField]
    private bool UseAltPrefab_;
    [SerializeField]
    private bool SmartTurns_;
    [SerializeField]
    private float DeviateChance_;
    [SerializeField]
    private float SplitChance_;
    [SerializeField]
    private int SplitLimit_;
    [SerializeField]
    private float ChangeDirectionChance_;
    [SerializeField]
    private float PathWidth_;
    [SerializeField]
    private float PathThickness_;
    [SerializeField]
    private float PillarSpace_;
    [SerializeField]
    private float PillarThicknessMin_;
    [SerializeField]
    private float PillarThicknessMax_;
    [SerializeField]
    private float PillarThicknessFirst_;
    [SerializeField]
    private Minmax Angle_;
    [SerializeField]
    private Minmax Slope_;
    [SerializeField]
    private Minmax Shadow_;
    [SerializeField]
    private PillarParameters PillarParameters_;
    [SerializeField]
    private PillarVisualParameters PillarVisualParameters_;

    public GameObject PathPrefab { get { return PathPrefab_; } }
    public GameObject PillarPrefab { get { return PillarPrefab_; } }
    public GameObject PathPrefabAlt { get { return PathPrefabAlt_; } }
    public GameObject PillarPrefabAlt { get { return PillarPrefabAlt_; } }
    public GameObject EndpointPrefab { get { return EndpointPrefab_; } }
    public Vector3 StartPosition { get { return StartPosition_; } }
    public float StartAngle { get { return StartAngle_; } }
    public bool Endpoints { get { return Endpoints_; } }
    public bool UseAltPrefab { get { return UseAltPrefab_; } }
    public bool SmartTurns { get { return SmartTurns_; } }
    public float DeviateChance { get { return DeviateChance_; } }
    public float SplitChance { get { return SplitChance_; } }
    public int SplitLimit { get { return SplitLimit_; } }
    public float ChangeDirectionChance { get { return ChangeDirectionChance_; } }
    public float PathWidth { get { return PathWidth_; } }
    public float PathThickness { get { return PathThickness_; } }
    public float PillarSpace { get { return PillarSpace_; } }
    public float PillarThicknessMin { get { return PillarThicknessMin_; } }
    public float PillarThicknessMax { get { return PillarThicknessMax_; } }
    public float PillarThicknessFirst { get { return PillarThicknessFirst_; } }
    public Minmax Angle { get { return Angle_; } }
    public Minmax Slope { get { return Slope_; } }
    public Minmax Shadow { get { return Shadow_; } }
    public PillarParameters PillarParameters { get { return PillarParameters_; } }
    public PillarVisualParameters PillarVisualParameters { get { return PillarVisualParameters_; } }

    public TowerParameters(GameObject pathprefab, GameObject pillarprefab, GameObject pathprefabalt, GameObject pillarprefabalt, GameObject endpointprefab, Vector3 startposition, float startangle, bool endpoints, bool usealtprefab, bool smartturns, float deviatechance, float splitchance, int splitlimit, float changedirectionchance, float pathwidth, float paththickness, float pillarspace, float pillarthicknessmin, float pillarthicknessmax, float pillarthicknessfirst, Minmax angle, Minmax slope, Minmax shadow, PillarParameters pillarparameters, PillarVisualParameters pillarvisualparameters)
    {
        PathPrefab_ = pathprefab;
        PillarPrefab_ = pillarprefab;
        PathPrefabAlt_ = pathprefabalt;
        PillarPrefabAlt_ = pillarprefabalt;
        EndpointPrefab_ = endpointprefab;
        StartPosition_ = startposition;
        StartAngle_ = startangle;
        Endpoints_ = endpoints;
        UseAltPrefab_ = usealtprefab;
        SmartTurns_ = smartturns;
        DeviateChance_ = deviatechance;
        SplitChance_ = splitchance;
        SplitLimit_ = splitlimit;
        ChangeDirectionChance_ = changedirectionchance;
        PathWidth_ = pathwidth;
        PathThickness_ = paththickness;
        PillarSpace_ = pillarspace;
        PillarThicknessMin_ = pillarthicknessmin;
        PillarThicknessMax_ = pillarthicknessmax;
        PillarThicknessFirst_ = pillarthicknessfirst;
        Angle_ = angle;
        Slope_ = slope;
        Shadow_ = shadow;
        PillarParameters_ = pillarparameters;
        PillarVisualParameters_ = pillarvisualparameters;
		
    }
	
	public void FromJsonObject(JsonParameters obj)
	{
        StartAngle_ = obj.StartAngle;
        Endpoints_ = obj.Endpoints;
        UseAltPrefab_ = obj.UseAltPrefab;
        SmartTurns_ = obj.SmartTurns;
        DeviateChance_ = obj.DeviateChance;
        SplitChance_ = obj.SplitChance;
        SplitLimit_ = obj.SplitLimit;
        ChangeDirectionChance_ = obj.ChangeDirectionChance;
        PathWidth_ = obj.PathWidth;
        PathThickness_ = obj.PathThickness;
        PillarSpace_ = obj.PillarSpace;
        PillarThicknessMin_ = obj.PillarThicknessMin;
        PillarThicknessMax_ = obj.PillarThicknessMax;
        PillarThicknessFirst_ = obj.PillarThicknessFirst;
        Angle_.FromJsonObject(obj.Angle);
        Slope_.FromJsonObject(obj.Slope);
        Shadow_.FromJsonObject(obj.Shadow);
        PillarParameters_.FromJsonObject(obj);
        PillarVisualParameters_.FromJsonObject(obj);
	}
    
    public override string ToString()
    {
        string ret = "Paths " + PathWidth_ + "x" + PathThickness_ + " ";
        if (Endpoints_) ret += "E ";
        if (UseAltPrefab_) ret += "U ";
        return ret + "\n" + PillarVisualParameters_ + "\n"
            + "Start " + StartPosition_ + " " + StartAngle_ + "\n"
            + "Angles " + Angle_ + "\n"
            + "Slopes " + Slope_ + "\n"
            + "Shadow " + Shadow_ + "\n"
            + "Pillar height " + "\n"
            + "Pillar thickness " + PillarThicknessMin_ + " to " + PillarThicknessMax_ + " [" + PillarThicknessFirst_ + "]\n"
            + "Pillar height " + PillarParameters_ + "\n"
            + "Pillar space " + PillarSpace_ + "\n"
            + "Deviate " + DeviateChance_ * 100 + (SmartTurns_ ? "% [S]\n" : "%\n")
            + "Split " + SplitChance_ * 100 + "%\n"
            + "Change direction " + ChangeDirectionChance_ * 100 + "%";
    }
}

public class JsonParameters
{
    public int Seed = 1;
    public bool RandomSeed = false;
    public float StartAngle = 0;
    public int PathCount = 400;
    public bool Endpoints = false;
    public bool UseAltPrefab = false;
    public bool SmartTurns = false;
    public float DeviateChance = 0.3f;
    public float SplitChance = 0.9f;
    public int SplitLimit = 4;
    public float ChangeDirectionChance = 0.05f;
    public float PathWidth = 1.8f;
    public float PathThickness = 0.5f;
    public float PillarSpace = 0.2f;
    public float PillarThicknessMin = 1;
    public float PillarThicknessMax = 2;
    public float PillarThicknessFirst = 3.5f;
	public MinmaxJSON Angle = new MinmaxJSON(25,35,15,10);
	public MinmaxJSON Slope = new MinmaxJSON(5,10,0,0);
	public MinmaxJSON Shadow = new MinmaxJSON(15,20,2,11);
    public float PillarMaxHeightMultiplier = 10;
    public float PilarMaxHeight = 40;
	public VisualsJsonParameters VisualsA = new VisualsJsonParameters();
	public VisualsJsonParameters VisualsB = new VisualsJsonParameters();
}

public class VisualsJsonParameters {
	public bool Bases = true;
    public bool Roofs = true;
    public bool Hide = false;
    public bool Extend = false;
    public bool RequireElevation = true;
    public float Required = 15;
    public bool ExtendInstead = true;
    public float SpareT = 5;
    public float SpareB = 5;
}

public class PillarChoiceParameters
{
    private Minmax Angle_;
    private Minmax Slope_;
    private Minmax Shadow_;

    public Minmax Angle { get { return Angle_; } }
    public Minmax Slope { get { return Slope_; } }
    public Minmax Shadow { get { return Shadow_; } }

    public PillarChoiceParameters(TowerParameters p)
    {
        Angle_ = p.Angle;
        Slope_ = p.Slope;
        Shadow_ = p.Shadow;
    }
}