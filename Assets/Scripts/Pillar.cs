﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Describes a pillar and its visual behavior.
public class Pillar
{
    private Util u = new Util();
    
    private PillarParameters p;
    
    private GameObject gameobject; //The GameObject this Pillar object represents
    private Vector2 location; //The coordinates of where it is standing
    private float thickness;
    private float bottom; //Bottom without accounting for SpareB
    private float top; //Top without accounting for SpareT
    private float maxtop; //The highest acceptable top for this pillar
    private int accesscount; //The number of paths winding around this pillar
    //How many left/right turns have so far been made around this pillar:
    private int lefts;
    private int rights;
    
    public Vector2 Location { get { return location; } }
    public float Thickness { get { return thickness; } }
    public float Bottom { get { return bottom; } }
    public float Top { get { return top; } }
    public float Maxtop { get { return maxtop; } }
    public bool isSimple { get { return accesscount == 1; } } //Used to choose the set of pillar visuals to use

    public float MaxHeight { get { return thickness * thickness * p.MaxHeightMultiplier + p.MaxHeight; } }

    public Pillar(PillarParameters pp, GameObject go, Vector2 l, float th, float bo, float to)
    {
        p = pp;
        gameobject = go;
        location = l;
        thickness = th;
        bottom = bo;
        top = to;
        maxtop = bo + MaxHeight;
        accesscount = 0;
        lefts = 0;
        rights = 0;
    }

    //LOGIC:
    
    //Recommend a direction based on what previous visits to this pillar have done:
    public bool RecommendDirection(bool currentdirection)
    {
        int ratio = lefts - rights;

        bool obey = true;
        if (ratio < 2 && ratio > -2)
        {
            obey = u.chance(0.5f);
        }
        if (ratio == 0) obey = false;

        if (obey) return (ratio < 0);
        //Otherwise no change:
        return currentdirection;
    }

    public void NewTop(float t)
    {
        top = t;
        accesscount++;
    }

    public void Visit(bool isleft)
    {
        if (isleft) lefts++;
        else rights++;
    }

    //VISUALS:

    public ToDoList ApplyVisuals(PillarVisualSubParameters pv)
    {
        ToDoList list = new ToDoList();

        if (tooLow(pv))
        {
            if (pv.ExtendInstead) extend();
        }
        else if (pv.Bases) list.Add(addBase(pv));
        if (pv.Roofs) list.Add(addRoof(pv));
        if (pv.Extend) extend();
        if (pv.Hide) hide();

        pillarUpdateDisplay(pv);

        return list;
    }
    private void hide()
    {
        gameobject.SetActive(false);
    }
    private Instantiator addBase(PillarVisualSubParameters pv)
    {
        return new Instantiator(pv.BasePrefab, u.v3(location, bottom - pv.SpareB), Quaternion.identity, "Pillar Base", gameobject.transform);
    }
    private Instantiator addRoof(PillarVisualSubParameters pv)
    {
        return new Instantiator(pv.RoofPrefab, u.v3(location, top + pv.SpareT), Quaternion.identity, "Pillar Roof", gameobject.transform);
    }
    private bool tooLow(PillarVisualSubParameters pv)
    {
        if (!pv.RequireElevation) return false;
        return bottom - pv.SpareB < pv.Required;
    }
    private void extend()
    {
        bottom = 0;
    }
    //Update the transform to fit the pillar's parameters:
    private void pillarUpdateDisplay(PillarVisualSubParameters pv)
    {
        float center = (bottom + top - pv.SpareB + pv.SpareT) / 2;
        float height = -bottom + top + pv.SpareB + pv.SpareT;
        gameobject.transform.position = new Vector3(location.x, center, location.y);
        gameobject.transform.localScale = new Vector3(thickness, height, thickness);
    }
}

//Describes all the pillars in a tower and is responsible for choosing targets to connect to.
public class PillarChoices
{
    private Util u = new Util();
    
    private PillarChoiceParameters p;
    private PillarVisualParameters pv;

    private List<Pillar> pillars;
    
    public int Count { get { return pillars.Count; } }

    public PillarChoices(PillarChoiceParameters pcp, PillarVisualParameters pvp)
    {
        p = pcp;
        pv = pvp;
        pillars = new List<Pillar>();
    }

    public void Add(Pillar q)
    {
        pillars.Add(q);
    }

    public void NewTop(Pillar q, float newtop)
    {
        q.NewTop(newtop);
    }

    //Retrieve a ToDoList array of all the decorations that need to be instantiated:
    public ToDoList[] Decorate()
    {
        ToDoList[] lists = new ToDoList[pillars.Count];

        for(int i = 0; i < pillars.Count; i++)
        {
            lists[i] = pillars[i].ApplyVisuals(chooseParameters(pillars[i]));
        }

        //Keep ToDoLists in an array instead of merging them. No need to waste processing power on that.
        return lists;
    }

    //Decide which set of parameters to use for a specific pillar:
    private PillarVisualSubParameters chooseParameters(Pillar q)
    {
        //Simple pillars are type A, complex are type B.
        //A pillar is simple exactly when it has exactly one path visiting it. (this condition could easily be altered)
        return (q.isSimple ? pv.VisualsA : pv.VisualsB);
    }

    //Find valid pillars to go to from the current position:
    public List<Pillar> FindValid(Sequence cur)
    {
        List<Pillar> valids = new List<Pillar>();

        foreach (Pillar p in pillars)
            if (isPillarValid(p, cur))
                valids.Add(p);

        return valids;
    }
    
    private bool isPillarValid(Pillar q, Sequence cur)
    {
        //Check the easy stuff:
        Vector2 directShadow = q.Location - u.v2(cur.Pos);
        float directShl = directShadow.magnitude;

        if (directShl < p.Shadow.MinMin) return false; //too close by
        if (directShl > p.Shadow.MaxMax) return false; //too far away
        if (cur.Pos.y + u.slopeToHeight(p.Slope.MinMin, directShl) > q.Maxtop) return false; //Too short
        if (cur.Pos.y + u.slopeToHeight(p.Slope.MaxMax, directShl) < q.Bottom) return false; //Too tall (in the sense of its bottom being too high up to reach)

        //Check the hard stuff (angles):
        float directAngle = u.vector2ToAngle(directShadow);
        float directAngleRelative = (directAngle - cur.Angle + 360) % 360;
        float minusDAR = (-directAngleRelative + 360) % 360;
        
        if (cur.GoingLeft)
            return (directAngleRelative > p.Angle.MinMin && directAngleRelative < p.Angle.MaxMax);
        else
            return (minusDAR > p.Angle.MinMin && minusDAR < p.Angle.MaxMax);
    }
}