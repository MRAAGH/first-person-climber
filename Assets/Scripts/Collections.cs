﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Stores a simple data structure which is common throughout this code.
//It contains the limits for when the value is being freely generated,
//and acceptable additional margins for when we are checking whether an existing value fits within limits.
[Serializable]
public class Minmax
{
    [SerializeField]
    private float Min_;
    [SerializeField]
    private float Max_;
    [SerializeField]
    private float MarginMin_;
    [SerializeField]
    private float MarginMax_;
    
    public float Min { get { return Min_; } }
    public float Max { get { return Max_; } }
    public float MarginMin { get { return MarginMin_; } }
    public float MarginMax { get { return MarginMax_; } }
    public float MinMin { get { return Min_ - MarginMin_; } }
    public float MaxMax { get { return Max_ + MarginMax_; } }

    public void Check(string name)
    {
        if (Min_ > Max_) Debug.LogError(name + " min is larger than max!");
        if (MarginMin_ < 0 || MarginMax_ < 0) Debug.LogError(name + " negative margin!");
    }
	
	public void FromJsonObject(MinmaxJSON obj)
	{
		Min_ = obj.min;
		Max_ = obj.max;
		MarginMin_ = obj.marginmin;
		MarginMax_ = obj.marginmax;
	}

    public override string ToString()
    {
        return Min_
            + (MarginMin_ == 0 ? "" : ("(" + (MarginMin_ < 0 ? "!+" + -MarginMin_ + "!" : "-" + MarginMin_) + ")"))
            + " to " + Max_
            + (MarginMax_ == 0 ? "" : ("(" + (MarginMax_ < 0 ? "!" + MarginMax_ + "!" : "+" + MarginMax_) + ")"));
    }
}

public class MinmaxJSON
{
	public float min = 10;
	public float max = 25;
	public float marginmin = 5;
	public float marginmax = 5;
	
	public MinmaxJSON(float a, float b, float c, float d)
	{
		min = a;
		max = b;
		marginmin = c;
		marginmax = d;
	}
}

//Carries all the data required to instantiate a GameObject.
//Used to transfer data from deeper classes to the surface.
public class Instantiator
{
    private GameObject Prefab_ { set; get; }
    private Transform Parent_ { set; get; }
    private Vector3 Position_ { set; get; }
    private Quaternion Rotation_ { set; get; }
    private string Name_ { set; get; }

    public GameObject Prefab { get { return Prefab_; } }
    public Transform Parent { get { return Parent_; } }
    public Vector3 Position { get { return Position_; } }
    public Quaternion Rotation { get { return Rotation_; } }
    public string Name { get { return Name_; } }

    public Instantiator(GameObject g, Vector3 po, Quaternion r, string n, Transform pa)
    {
        Prefab_ = g;
        Position_ = po;
        Rotation_ = r;
        Name_ = n;
        Parent_ = pa;
    }
}
//Holds a list of instantiators while they are carried to a class which is capable of instantiation.
public class ToDoList
{
    private List<Instantiator> list;
    private int progress;

    public bool IsDone { get { return progress == list.Count; } }
    public Instantiator Next { get { progress++; return list[progress - 1]; } }

    public ToDoList()
    {
        list = new List<Instantiator>();
        progress = 0;
    }

    public void Add(Instantiator i)
    {
        list.Add(i);
    }
}

//A set of variables to describe the status of one generation sequence.
//No access limitations as this is but a dumb container.
public class Sequence
{
    public float Angle { set; get; } //2D
    public Vector3 Pos { set; get; } //The position of the end of the last generated path of this sequence
    public bool GoingLeft { set; get; } //On which side of the pillar we are not

    //Those at a lower altitude get priority:
    public float Rating { get { return -Pos.y; } }

    public Sequence(float angle, Vector3 pos, bool goingleft)
    {
        Angle = angle;
        Pos = pos;
        GoingLeft = goingleft;
    }
}
//All generation sequences in a specific tower.
public class SequencePriorityQueue
{
    //Priority queue currently implemented as a simple array:
    private List<Sequence> list;
    public int Count { get { return list.Count; } }

    public SequencePriorityQueue()
    {
        list = new List<Sequence>();
    }

    public Sequence Pop()
    {
        //Find min element:
        Sequence min = list[0];
        foreach (Sequence v in list)
        {
            if (v.Rating > min.Rating)
            {
                min = v;
            }
        }
        return min;

        //In this case, Pop() does not remove the element, it just returns a reference to it.
    }

    public void Push(Sequence v)
    {
        list.Add(v);
    }

    public int Size()
    {
        return list.Count;
    }
}
