﻿using UnityEngine;
using System.Collections;

public class PlayerObserver : MonoBehaviour {
    
    private Vector3 pos;
    private Quaternion rot;
    private Vector3 vel;

    void Start() {
        pos = transform.position;
    }

    void Update()
    {
        vel = transform.position - pos;
        pos = transform.position;
    }
	
    public string report()
    {
        rot = transform.rotation;
        return "Position " + pos + "\nVelocity " + vel * 30 + "\nRotation " + rot.eulerAngles;
    }
}
