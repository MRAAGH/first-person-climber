﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

//The manager of terrain generation. This is the class that actually gets serialized in the Inspector.
//Others are just underlying data structures so to speak. Except for parameter classes.

public class RandomTerrainGen : MonoBehaviour
{
    [SerializeField]
    private int Seed;
    [SerializeField]
    private bool RandomSeed;

    [SerializeField]
    private Transform TerrainSpawn;
    [SerializeField]
    private GameObject TowerPrefab;
    [SerializeField]
    private int PathCount;
    [SerializeField]
    private TowerParameters TowerParameters;
    
    private List<TowerGenerator> towers;

    void Start ()
    {
		
		
		
		string[] contents = File.ReadAllLines("config.txt");
		string jsonstring = string.Join("", contents);
		JsonParameters obj = JsonUtility.FromJson<JsonParameters>(jsonstring);
		Debug.Log(obj.Seed);
		Seed = obj.Seed;
		RandomSeed = obj.RandomSeed;
		PathCount = obj.PathCount;
		this.TowerParameters.FromJsonObject(obj);
		
		
		
        if (!RandomSeed)
            Random.seed = Seed;
        Seed = Random.seed;

        towers = new List<TowerGenerator>();
        //Make a tower:
        GameObject atower = (GameObject) Instantiate(TowerPrefab, TerrainSpawn.position, TerrainSpawn.rotation);
        atower.transform.parent = TerrainSpawn;
        atower.name = "Tower";
        TowerGenerator tg = atower.GetComponent<TowerGenerator>();
        towers.Add(tg);

        //Begin:
        tg.Initialize(TowerParameters);
        tg.FirstStep();

        //Generate:
        for (int i = 0; i < PathCount - 1; i++)
        {
            foreach (TowerGenerator t in towers)
            {
                t.NextStep();
            }
        }

        //Decorate
        foreach (TowerGenerator t in towers)
        {
            t.Decorate();
        }
    }

    public override string ToString()
    {
        string rep = "Seed: " + Seed + "\n";
        rep += "Towers: " + towers.Count + "\n";
        int i = 0;
        foreach(TowerGenerator t in towers)
        {
            rep += "Tower #" + i + "\n" + t + "\n\n";
            i++;
        }
        return rep;
    }
}
